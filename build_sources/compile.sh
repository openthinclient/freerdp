#!/bin/bash

# Build script for FreeRDP binaries.
# Applies custom patches, builds and installs to $DESTDIR/opt/freerdp

set -e

cd $SRCDIR/build_sources/FreeRDP

if [ -z "$(git status --short)" ]; then
    echo Applying patches
    git apply ../FreeRDP.patches/*
    PATCHES_APPLIED=true
else
    echo Skipping patches - build_sources/FreeRDP is dirty
    PATCHES_APPLIED=false
fi

# Configure build
cmake -GNinja \
    -DBUILD_TESTING=OFF \
    -DBUILTIN_CHANNELS=ON \
    -DHAVE_AIO_H=1 \
    -DHAVE_EXECINFO_H=1 \
    -DHAVE_FCNTL_H=1 \
    -DHAVE_INTTYPES_H=1 \
    -DHAVE_MATH_C99_LONG_DOUBLE=1 \
    -DHAVE_POLL_H=1 \
    -DHAVE_PTHREAD_MUTEX_TIMEDLOCK=ON \
    -DHAVE_PTHREAD_MUTEX_TIMEDLOCK_LIB=1 \
    -DHAVE_PTHREAD_MUTEX_TIMEDLOCK_SYMBOL= \
    -DHAVE_SYSLOG_H=1 \
    -DHAVE_SYS_EVENTFD_H=1 \
    -DHAVE_SYS_FILIO_H= \
    -DHAVE_SYS_MODEM_H= \
    -DHAVE_SYS_SELECT_H=1 \
    -DHAVE_SYS_SOCKIO_H= \
    -DHAVE_SYS_STRTIO_H= \
    -DHAVE_SYS_TIMERFD_H=1 \
    -DHAVE_TM_GMTOFF=1 \
    -DHAVE_UNISTD_H=1 \
    -DHAVE_XI_TOUCH_CLASS=1 \
    -DWITH_ALSA=ON \
    -DWITH_CCACHE=ON \
    -DWITH_CHANNELS=ON \
    -DWITH_CLIENT=ON \
    -DWITH_CLIENT_AVAILABLE=1 \
    -DWITH_CLIENT_CHANNELS=ON \
    -DWITH_CLIENT_CHANNELS_AVAILABLE=1 \
    -DWITH_CLIENT_COMMON=ON \
    -DWITH_CLIENT_INTERFACE=OFF \
    -DWITH_CUPS=ON \
    -DWITH_DEBUG_ALL=OFF \
    -DWITH_DSP_EXPERIMENTAL=OFF \
    -DWITH_DSP_FFMPEG=ON \
    -DWITH_EVENTFD_READ_WRITE=1 \
    -DWITH_FAAC=OFF \
    -DWITH_FAAD2=OFF \
    -DWITH_FFMPEG=TRUE \
    -DWITH_GFX_H264=ON \
    -DWITH_GPROF=OFF \
    -DWITH_GSM=OFF \
    -DWITH_GSSAPI=OFF \
    -DWITH_GSTREAMER_1_0=ON \
    -DWITH_ICU=OFF \
    -DWITH_IPP=OFF \
    -DWITH_JPEG=OFF \
    -DWITH_LAME=OFF \
    -DWITH_LIBRARY_VERSIONING=ON \
    -DWITH_LIBSYSTEMD=ON \
    -DWITH_MACAUDIO=OFF \
    -DWITH_MACAUDIO_AVAILABLE=0 \
    -DWITH_MANPAGES=ON \
    -DWITH_MBEDTLS=OFF \
    -DWITH_OPENH264=OFF \
    -DWITH_OPENSLES=OFF \
    -DWITH_OPENSSL=ON \
    -DWITH_OSS=ON \
    -DWITH_PAM=ON \
    -DWITH_PCSC=ON \
    -DWITH_PROFILER=OFF \
    -DWITH_PULSE=ON \
    -DWITH_SAMPLE=OFF \
    -DWITH_SANITIZE_ADDRESS=OFF \
    -DWITH_SANITIZE_ADDRESS_AVAILABLE=0 \
    -DWITH_SANITIZE_MEMORY=OFF \
    -DWITH_SANITIZE_MEMORY_AVAILABLE=0 \
    -DWITH_SANITIZE_THREAD=OFF \
    -DWITH_SANITIZE_THREAD_AVAILABLE=0 \
    -DWITH_SERVER=ON \
    -DWITH_SERVER_CHANNELS=ON \
    -DWITH_SERVER_INTERFACE=ON \
    -DWITH_SMARTCARD_INSPECT=OFF \
    -DWITH_SOXR=OFF \
    -DWITH_SSE2=ON \
    -DWITH_THIRD_PARTY=OFF \
    -DWITH_VALGRIND_MEMCHECK=OFF \
    -DWITH_VALGRIND_MEMCHECK_AVAILABLE=0 \
    -DWITH_WAYLAND=OFF \
    -DWITH_X11=ON \
    -DWITH_X264=OFF \
    -DWITH_XCURSOR=ON \
    -DWITH_XDAMAGE=ON \
    -DWITH_XEXT=ON \
    -DWITH_XFIXES=ON \
    -DWITH_XI=ON \
    -DWITH_XINERAMA=ON \
    -DWITH_XKBFILE=ON \
    -DWITH_XRANDR=ON \
    -DWITH_XRENDER=ON \
    -DWITH_XSHM=ON \
    -DWITH_XTEST=ON \
    -DWITH_XV=ON \
    -DWITH_ZLIB=ON \
    -DCHANNEL_URBDRC=ON \
    -DCHANNEL_URBDRC_CLIENT=ON \
    -DWITH_CAIRO=ON \
    -DCMAKE_INSTALL_PREFIX=/opt/freerdp

# Build and install FreeRDP to $DESTDIR/opt/freerdp
cmake --build . --target install

$PATCHES_APPLIED && git apply --reverse ../FreeRDP.patches/*

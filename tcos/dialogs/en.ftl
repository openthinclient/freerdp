# ----------------------------------------------------------------------------
# Info messages
# ----------------------------------------------------------------------------

info-title = FreeRDP

conn_closed = The connection has been closed by the RDP server.

    (Exit code 1)

sess_closed = Your session has been closed by the RDP server.

    (Exit code 2)

idle_timeout = Your session has been closed by the RDP server due to inactivity.

    (Exit code 3)

logon_timeout = The logon has been canceled by the RDP server due to inactivity.

    (Exit code 4)

conn_replaced = Your session has been taken over by another client.

    (Exit code 5)

# ----------------------------------------------------------------------------
# Login dialog messages
# ----------------------------------------------------------------------------

login-title = FreeRDP Login
gateway-login-title = FreeRDP Gateway Login

# ----------------------------------------------------------------------------
# Login error messages
# ----------------------------------------------------------------------------

login-error-title = FreeRDP login error

auth-failure = Authentication failure.

    Please check your credentials.

logon-failure = Logon failure. Please check credentials and connection to the server.

    If the problem persists, please contact your administrator.

# ----------------------------------------------------------------------------
# Configuration error messages
# ----------------------------------------------------------------------------

configuration-error-title = FreeRDP Configuration error

commandline-error = FreeRDP could not parse the command line.

    The option "Additional parameters" in the openthinclient management should be checked for errors.

    Details:

    {$error}


    Please contact your administrator.

wrong_suppress_exit_codes = Please check the "suppress exit codes" setting. Only a comma seperated list of integer numbers is accepted. The suppression of error dialogs won't work.

    Please contact your administrator.

invalid_hostname = Invalid Server Name '{$hostname}'!

no_host = No Server name given!

    Please contact your administrator.

no_domain = No Domain given!

error_unknown_var = Unknown variable "${$var}".

    Please contact your administrator.

wrong_stepover_port = Please check the "StepOver TCP connection port" setting.

    Falling back to default value 8888.

    Please contact your administrator.

# ----------------------------------------------------------------------------
# Error messages
# ----------------------------------------------------------------------------

error-title = FreeRDP Error

error_stdin_login_data = No login data passed.

    Please contact your administrator.

error_sso_login_data = Single sign-on failed.

    Could not retrieve password and username.

    Please make sure that you have a "Single Sign-on" device, but not a "Autologin" device assigned to the thin client in the openthinclient management and you are not using .

    Please contact your administrator.

invalid_domain = Invalid domain specified. Domain {$domain} is predefined.

no_gw_domain = No Gateway domain given!

general-error = An error occurred.

    Exit code: {$returncode}

    Please try again. If the problem persists, please contact your administrator.

conn_failed = Could not connect to the RDP server.

    Please contact your administrator.

nego_failed = The connection negotiation failed.

    The setting "Protocol security" in the openthinclient management is not compatible with the settings of the RDP server.

    Please contact your administrator.

error_uncaught = An unexpected error occurred.

    {$error}

    Please contact your administrator.

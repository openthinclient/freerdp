# ----------------------------------------------------------------------------
# Info messages
# ----------------------------------------------------------------------------

info-title = FreeRDP

conn_closed = Die Verbindung wurde vom RDP-Server geschlossen.

    (Exit Code 1)

sess_closed = Ihre Sitzung wurde vom RDP-Server beendet.

    (Exit Code 2)

idle_timeout = Ihre Sitzung wurde vom RDP-Server aufgrund von Inaktivität beendet.

    (Exit Code 3)

logon_timeout = Die Anmeldung wurde vom RDP-Server aufgrund von Inaktivität beendet.

    (Exit Code 4)

conn_replaced = Ihre Sitzung wurde von einem anderen Client übernommen.

    (Exit Code 5)

# ----------------------------------------------------------------------------
# Login dialog messages
# ----------------------------------------------------------------------------

login-title = FreeRDP Anmeldung
gateway-login-title = FreeRDP Gateway Anmeldung

# ----------------------------------------------------------------------------
# Login error messages
# ----------------------------------------------------------------------------

login-error-title = FreeRDP Anmeldefehler

auth-failure = Authentifizierung fehlgeschlagen.

    Bitte überprüfen Sie Ihre Anmeldedaten.

logon-failure = Anmeldung fehlgeschlagen. Bitte überprüfen Sie die Anmeldedaten und die Verbindung zum Server.

    Sollte das Problem anhalten, wenden Sie sich bitte an Ihren Administrator.

# ----------------------------------------------------------------------------
# Configuration error messages
# ----------------------------------------------------------------------------

configuration-error-title = FreeRDP Konfigurationsfehler

commandline-error = FreeRDP konnte die Kommandozeilenparameter nicht
    verarbeiten.

    Die Einstellung "Zusätzliche Parameter" im openthinclient-Management sollte auf Fehler geprüft werden.

    Details:

    {$error}


    Bitte informieren Sie Ihren Administrator.

wrong_suppress_exit_codes = Bitte überprüfen Sie die Einstellung bzgl. der zu unterdrückenden Exit Codes. Sie müssen eine kommaseparierte Liste von Integer Zahlen angeben. Die Unterdrückung der Fehlerdialoge wird nicht funktionieren.

    Bitte informieren Sie Ihren Administrator.

invalid_hostname = Ungültiger Servername '{$hostname}'

no_host = Kein Servername angegeben.

    Bitte informieren Sie Ihren Administrator.

no_domain = Keine Domäne angegeben.

error_unknown_var = Unbekannte Variable "${$var}".

    Bitte informieren Sie Ihren Administrator.

wrong_stepover_port = Bitte überprüfen Sie die Einstellung "StepOver TCP Connection Port"

    Falle zurück auf Standard-Wert 8888.

    Bitte informieren Sie Ihren Administrator.

# ----------------------------------------------------------------------------
# Error messages
# ----------------------------------------------------------------------------

error-title = FreeRDP Fehler

error_stdin_login_data = Es wurden keine Anmeldeinformationen übergeben.

    Bitte informieren Sie Ihren Administrator.

error_sso_login_data = Single Sign-on gescheitert.

    Benutzername und Passwort konnten nicht ausgelesen werden.

    Bitte überprüfen Sie, dass dem ThinClient ein "Single Sign-on" Gerät, aber kein "Autologin" Gerät im openthinclient-Management zugewiesen ist.

    Bitte informieren Sie Ihren Administrator.

invalid_domain = Ungültige Domäne angegeben. Domäne {$domain} ist voreingestellt.

no_gw_domain = Keine Gateway-Domäne angegeben.

general-error = Ein Fehler ist aufgetreten.

    Exit Code: {$returncode}

    Versuchen Sie es erneut oder wenden Sie sich an Ihren Administrator falls das Problem weiterhin besteht.

conn_failed = Konnte keine Verbindung zum RDP-Server herstellen.

    Bitte informieren Sie Ihren Administrator.

nego_failed = Die Verhandlung der Sicherheitsebene ist fehlgeschlagen.

    Die Einstellung "Sicherheitsprotokoll" im openthinclient-Management ist nicht kompatibel mit den Einstellungen des RDP-Servers.

    Bitte informieren Sie Ihren Administrator.

error_uncaught = Ein unerwarteter Fehler ist aufgetreten.

    {$error}

    Bitte informieren Sie Ihren Administrator.

#!/bin/bash

# Build FreeRDP binaries inside container and move to package-rootfs

# The container directory can be given as parameter and is meant to be
# base.sfs.cache-pre-dev/ from the os-core10 repo (needs to be extracted
# from base.sfs.cache.tar.gz).

[ "$EUID" -eq 0 ] || exec sudo "$0" "$@"
cd $(dirname $0)


CONTAINER_DIR=${1:-../os-core10/base.sfs.cache-pre-dev}
mkdir -p .build

echo Run build
sudo systemd-nspawn \
    --directory $CONTAINER_DIR \
    --tmpfs=/run/lock \
    --bind=$(pwd):/root/src \
    --setenv=SRCDIR=/root/src \
    --bind=$(pwd)/.build:/root/dest \
    --setenv=DESTDIR=/root/dest \
    /root/src/build_sources/compile.sh


# Give ownership to current user so we don't need to sudo git or sudo make clean
OWNER=$(who am i | cut -d' ' -f1)
echo Set owner to $OWNER
chown $OWNER:$OWNER .build
chown $OWNER:$OWNER .build/opt
chown -R $OWNER:$OWNER .build/opt/freerdp

echo Move binaries to package-rootfs
rm -r package-rootfs/opt/freerdp/*
sudo -u $OWNER mkdir -p package-rootfs/opt/freerdp/{bin,lib}
cp -dp .build/opt/freerdp/bin/* package-rootfs/opt/freerdp/bin
cp -dp .build/opt/freerdp/lib/* package-rootfs/opt/freerdp/lib 2> /dev/null

echo Done
